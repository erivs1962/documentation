Welcome to Eric V.'s documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   kubetraining
   kubeadvanced
   onprem-cloudagnostic-k8s
   prometheus_monitoring
   kubepackaging
   kubeserverless
   microservices
   certmanager
   kubectl


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
