########################
Serverless on Kubernetes
########################
What is serverless?
-------------------
• Public Cloud providers often provide Serverless capabilities in which you can deploy functions, rather than instances or containers

  • Azure Functions
  • AWS Lambda
  • Google Cloud Functions

• With these products, you don't need to manage the underlying infrastructure
• The functions are also not "always-on" unlike containers and instances, which can greatly reduce cost of serverless if the function doesn't need to be executed a lot
• Serverless in public cloud can reduce the complexity, operational cost and engineering time to get code running

  • You don't need to manage a Windows/Linux distribution
  • You don't need to build containers
  • You only pay for the time your function is running
  • A developer can "just push" the code and does not have to worry about many operational aspects

    • Although "cold-starts", the time for a function to start after it has not been invoked for some time, can be a
      operational issue that needs to be taken care of

• This is an example of a AWS Lambda Function:

.. code-block:: bash

   exports.handler = function(event, context) {
     context.succeed("Hello, World");
   };

• You'd still need to setup when the code is being executed
• For example, in AWS you would use the API Gateway to setup a URL that will invoke this function when visited
• Rather than using containers to start applications on Kubernetes, you can also use Functions
• Currently the most popular projects enabling functions are:

  • OpenFaas
  • Kubeless
  • Fission
  • OpenWhisk

• You can install and use any of the projects to let developers launch functions on your Kubernetes cluster
• As an administrator, you'll still need to manage the underlying infrastructure, but from a developer standpoint,
  he/she will be able to quickly and easily deploy functions on Kubernetes
• All these projects are pretty new, so their feature set will drastically change over time

  • If you're looking to adopt a serverless technology for your Kubernetes cluster, it's best to compare the features
    and the maturity of multiple software products and make your own decision

• Kubeless is the technology specifically written for Kubernetes and is easy to setup and use

Kubeless
--------
• Kubeless is a Kubernetes-native framework (source: https://github.com/kubeless/kubeless)

  • It leverages the Kubernetes resources to provide auto-scaling, API routing, monitoring, etc...

• It uses Custom Resource Definitions to be able to create functions
• It's open source and not affiliated with any commercial organization
• It also has a UI available for developers to deploy functions
• With Kubeless you deploy a function in your preferred language
• Currently the following runtimes are supported:

  • Python
  • NodeJS
  • Ruby
  • PHP
  • .NET
  • Golang
  • Others

• Once you've deployed your function, you'll need to determine how it will be triggered
• Currently the following functions are supported:

  • HTTP functions

    • HTTP functions get executed when an HTTP endpoint is triggered
    • You write a function and return the text/HTML that needs to be displayed in the browser

  • Scheduled function
  • PubSub (Kafka or NATS)

    • Triggers a function when data is available in Kafka / NATS

  • AWS Kinesis

    • Triggers based on data in AWS Kinesis (similar to Kafka)