#############
Microservices
#############
• Kubernetes makes it easy to deploy a lot of diverse applications
• Those applications can be monoliths that don't have anything to do with each other, or microservices, small
  services that make up one application
• The microservices architecture is increasingly popular
• This approach allows developers to split up the application in multiple independent parts
• Having to manage microservices can put an operational strain on the engineering team
• Between microservices there is usually:

  • No encryption
  • No retries, no failover
  • No intelligent load balancing
  • No routing decisions
  • No metrics / logs / traces
  • No access control

• Work around this by using a proxy container, called a Sidecar, which can give you:

  • Encryption between apps
  • Retries when connection fails
  • Intelligent load balancing
  •

• You can add a management interface for the Sidecars and have:

  • Routing decisions
  • Metrics / logs / traces
  • Access control

The above you can automate with Istio (https://istio.io/docs/concepts/what-is-istio/)