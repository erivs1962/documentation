##################################
Kubernetes packaging and deploying
##################################

****
Helm
****
• Helm is the best way to find, share and use software built for Kubernetes
• Helm is a package manager for Kubernetes
• It helps you to manage Kubernetes applications
• Helm is maintainted by the CNCF - The Cloud Native Computing Foundation (together with Kubernetes, fluentd, linkderd, and others)

  • Is's now maintained in collaboration with Microsoft, Google, Bitnami and the helm contributor community.

• To start using helm, you first need to download the helm client (https://github.com/helm/helm/releases/tag/v2.11.0)
• You need to run ``helm init`` to initialize helm on the kubernetes cluster

  • This will install Tiller
  • If you have RBAC installed (recent clusters have it enabled now by default), you'll also need to add a ServiceAccount and RBAC rules

• After this, helm is ready to use, and you can start installing charts

******
Charts
******
• Helm uses a packaging format called charts

  • A chart is a collection of files that describe a set of Kubernetes resources
  • A single chart can deploy an app, a piece of software or a database for example
  • It can have dependencies, e.g. to install wordpress chart, you need a mysql chart
  • You can write your own chart to deploy your application on Kubernetes using helm

• Charts use templates that are typically developed by a package maintainer
• They will generate yaml files that Kubernetes understands
• You can think of templates as dynamic yaml files, which can contain logic and variables
• This is an example of a template within a chart:

.. code-block:: bash

   apiVersion: v1
   kind: ConfigMap
   metadata:
     name: {{ .Release.Name }}-configmap
   data:
     myvalue: "Hello World"
     drink: {{ .Values.favoriteDrink }}

• The ``favoriteDrink`` value can then be overridden by the user when running helm install

********************
Helm service account
********************

.. code-block:: bash

   apiVersion: v1
   kind: ServiceAccount
   metadata:
     name: tiller
     namespace: kube-system
   ---
   apiVersion: rbac.authorization.k8s.io/v1beta1
   kind: ClusterRoleBinding
   metadata:
     name: tiller
   roleRef:
     apiGroup: rbac.authorization.k8s.io
     kind: ClusterRole
     name: cluster-admin
   subject:
     - kind: ServiceAccount
       name: tiller
       namespace: kube-system

***************
Common commands
***************

=============  ========================================
**Command**    **Description**
helm init      Install tiller on the cluster
helm reset     Remove tiller from the cluster
helm install   Install a helm chart
helm search    search for a chart
helm list      list releases (installed charts)
helm upgrade   upgrade a release
helm rollback  rollback a release to a previous version
=============  ========================================

**********************
Create your own charts
**********************

• You can create helm charts to deploy your own apps
• It's the recommended way to deploy your applications on Kubernetes

  • Packaging the app allows you to deploy the app in 1 command (instead of using kubectl create / apply)
  • Helm allows for upgrades and rollbacks
  • Your helm chart is version controlled

• To create the files necessary for a new chart, you can run the command:

.. code-block:: bash

   helm create <mychart>

• This will create a directory structure <mychart> and some files and subdirectories.

