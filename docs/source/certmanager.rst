####################################
Managing TLS certs with cert-manager
####################################
• If you want to use a secure http connection (https), you need to have certificates
• Those certificates can be bought or can be issued by some public cloud providers like AWS's Certificate Manager
• Managing SSL/TLS certificates yourself often takes a lot of time and are time consuming to install and extend

  • You also cannot issue your own certificates for production websites as they are not trusted by the common
    internet browsers (Chrome, IE, ...)

• Cert-manager can ease the issuing of certificates and their management
• Cert-manager can use LetsEncrypt
• LetsEncrypt is a free, automated and open Certificate Authority

  • LetsEncrypt can issue certificates for free for your app or website
  • You'll need to prove to LetsEncrypt that you are the owner of a domain
  • After that they'll issue a certificate for you

• The certificate is recognized by major software verndors and browsers
• Cert-manager can automate the verification process for LetsEncrypt
• With LetsEncrypt you'll also have to renew certificates every couple of months
• Cert-manager will periodically check the validity of the certificates and will start the renewal process if necessary
• LetsEncrypt in combination with cert-manager takes away a lot of hassle in dealing with certificates, allowing you
  to secure your endpoints in an easy and affordable way
• You can only issue certificates for a domain name you own
• You'll need to have a domain name like xyz.com

  • If you were using a domain name to bring up your cluster you can re-use that domain
  • Otherwise you can get one for free from www.dot.tk or other providers
  • Or you can buy one from any provider that sells domain names, GoDaddy, CloudFlare, AWS Route 53, ....
  • Less popular extensions only cost a few dollars
